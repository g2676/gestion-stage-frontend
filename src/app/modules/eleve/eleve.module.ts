import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EleveRoutingModule } from './eleve-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EleveRoutingModule
  ]
})
export class EleveModule { }
