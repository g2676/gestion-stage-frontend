import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionsListeComponent } from './institutions-liste.component';

describe('InstitutionsListeComponent', () => {
  let component: InstitutionsListeComponent;
  let fixture: ComponentFixture<InstitutionsListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstitutionsListeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionsListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
