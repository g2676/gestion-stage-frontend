import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InstitutionsComponent } from './institutions/institutions.component';
import { StagiaireDetailsComponent } from './stagiaire-details/stagiaire-details.component';
import { StagiairesListeComponent } from './stagiaires-liste/stagiaires-liste.component';

const routes: Routes = [
  { path: '', component:  InstitutionsComponent },
  {path: "stagiaires", component :StagiairesListeComponent},
  {path: "stagiaires/:id", component :StagiaireDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstitutionsRoutingModule { }
