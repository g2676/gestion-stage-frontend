import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InstitutionsRoutingModule } from './institutions-routing.module';
import { StagiairesListeComponent } from './stagiaires-liste/stagiaires-liste.component';
import { InstitutionsComponent } from './institutions/institutions.component';
import { StagiaireDetailsComponent } from './stagiaire-details/stagiaire-details.component';


@NgModule({
  declarations: [
    StagiairesListeComponent,
    InstitutionsComponent,
    StagiaireDetailsComponent
  ],
  imports: [
    CommonModule,
    InstitutionsRoutingModule
  ]
})
export class InstitutionsModule { }
