import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { InstitutionDetailsComponent } from './institution-details/institution-details.component';
import { InstitutionsListeComponent } from './institutions-liste/institutions-liste.component';
import { StatistiqueComponent } from './statistique/statistique.component';


const routes: Routes = [
  { path: 'admin', component: AdminComponent },
  {path: "institutions", component :InstitutionsListeComponent},
  {path: "institutions/:id", component : InstitutionDetailsComponent},
  { path: 'statistique', component: StatistiqueComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
