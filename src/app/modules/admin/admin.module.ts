import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { InstitutionDetailsComponent } from './institution-details/institution-details.component';
import { InstitutionsListeComponent } from './institutions-liste/institutions-liste.component';
import { StatistiqueComponent } from './statistique/statistique.component';
import { AdminComponent } from './admin/admin.component';


@NgModule({
  declarations: [
    InstitutionDetailsComponent,
    InstitutionsListeComponent,
    StatistiqueComponent,
    AdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
